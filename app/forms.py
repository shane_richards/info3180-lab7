from flask.ext.wtf import Form
from wtforms import TextField, BooleanField
from wtforms.validators import Email, Required

class LoginForm(Form):
    email = TextField('email', validators=[Required()])
    password = TextField('password', validators=[Required()])
    # remember_me = TextFieldField('remember_me', default=False)

class RegisterForm(Form):
	first_name = TextField('first_name', validators=[Required()])
	last_name = TextField('last_name', validators=[Required()])
	email = TextField('email', validators=[Required(), Email()])
	password = TextField('password', validators=[Required()])
	confirmation = TextField('confirmation', validators=[Required()])

