'use strict';

(function(angular){
	angular
		.module('wishlist')
		.controller('LogoutCtrl',
			['$http', '$scope', '$state', '$log', 'localStorageService', 
			logoutCtrl]);

	function logoutCtrl($http, $scope, $state, $log, localStorageService) {
		var token = localStorageService.get('token');

		$http.get('/api/logout', {headers: {
			'auth-token': token
		}}).then(function(response) {
			$log.log(response);
			localStorageService.clearAll();
			$scope.$broadcast('user.logout');
			$state.go('home');
		}, function(error) {
			$log.log(error);
		});
	}
})(angular);