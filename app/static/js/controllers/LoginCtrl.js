'use strict';

(function(angular){
	angular
		.module('wishlist')
		.controller('LoginCtrl',
			['$scope', '$log', '$http', 'localStorageService', '$state',
			loginCtrl]);

	function loginCtrl($scope, $log, $http, localStorageService, $state) {
		if (localStorageService.get('token') != null) {
			$state.go('home');
		}

		$scope.loginUser = function(user) {
			$http.post('/api/login', user)
				.then(function(response) {
					var data = response.data;
					localStorageService.set('token', data.token);
					localStorageService.set('firstName', data.first_name);
					localStorageService.set('id', data.id);
					$scope.$emit('user.login');
					$state.go('home');
				}, function(error) {
					$log.log(error);
				});
		};
	}
	
})(angular);