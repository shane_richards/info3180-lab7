'use strict';

(function(angular) {
	angular
		.module('wishlist', ['ui.router', 'LocalStorageModule'])
		.config(['$stateProvider', '$urlRouterProvider', 
			'localStorageServiceProvider', '$httpProvider', routesConfiguration]);

	function routesConfiguration($stateProvider, $urlRouterProvider, 
		localStorageServiceProvider, $httpProvider) {

		function templatePath(path) {
			return 'partials/' + path + '.html';
		}

		localStorageServiceProvider.setNotify(true, true);

		$urlRouterProvider.otherwise('/');

		$stateProvider
			.state('home', {
				url: '/',
				templateUrl: templatePath('pages/home')
			})
			.state('about', {
				url: '/about',
				templateUrl: templatePath('pages/about')
			})
			.state('login', {
				url: '/login',
				templateUrl: templatePath('session/login'),
				controller: 'LoginCtrl'
			})
			.state('register', {
				url: '/register',
				templateUrl: templatePath('session/register'),
				controller: 'RegisterCtrl'
			})
			.state('logout', {
				url: '/logout',
				template: '<h1>Signing out</h1>',
				controller: 'LogoutCtrl'
			})
			.state('show_profile', {
				url: '/profiles/{userId}',
				templateUrl: templatePath('profiles/show'),
				controller: 'ProfileShowCtrl'
			})
			.state('new_wishlist', {
				url: '/wishlists/new',
				templateUrl: templatePath('wishlists/new'),
				controller: 'WishlistNewCtrl'
			})
			.state('show_wishlist', {
				url: '/profiles/{userId}/wishlists/{wishlistNo}',
				templateUrl: templatePath('wishlists/show'),
				controller: 'WishlistShowCtrl'
			})
			.state('all_wishlist', {
				url: '/profiles/{userId}/wishlists',
				templateUrl: templatePath('wishlists/all'),
				controller: 'WishlistAllCtrl'
			});
	}
})(angular);