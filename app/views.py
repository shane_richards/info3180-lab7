"""
Flask Documentation:     http://flask.pocoo.org/docs/
Jinja2 Documentation:    http://jinja.pocoo.org/2/documentation/
Werkzeug Documentation:  http://werkzeug.pocoo.org/documentation/

This file creates your application.
"""

from flask import render_template, request, redirect, url_for,jsonify,g,session

from werkzeug.datastructures import MultiDict
from app.models import User, AuthToken
from app.forms import LoginForm, RegisterForm

from flask.ext.login import login_user, logout_user, current_user, login_required
from sqlalchemy.exc import IntegrityError
from app import app, db, lm, oid

@app.before_request
def before_request():
    g.user = current_user

@app.route('/')
@app.route('/home')
def home():
    """Render website's home page."""
    return app.send_static_file('index.html')

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def assets(path):
    """Respond with static files"""
    return app.send_static_file(path)

@app.route('/api/register', methods=['POST'])
def register():
    data = MultiDict(mapping=request.json)
    form = RegisterForm(data, csrf_enabled=False)

    # if not form.validate():
    #     return jsonify({'error': 'please correct the form'})

    if data.get('password') != data.get('confirmation'):
        return jsonify({'error': 'password and confirmation do not match'})

    auth = AuthToken()

    new_user = User(first_name=data.get('firstName'),
                    last_name=data.get('lastName'),
                    email=data.get('email'),
                    password=data.get('password'))

    new_user.tokens.append(auth)

    try:
        db.session.add(new_user)
        db.session.add(auth)
        db.session.commit()
    except IntegrityError as e:
        return jsonify({'error': 'email already taken'})

    return jsonify({
        'id': new_user.id,
        'first_name': new_user.first_name,
        'last_name': new_user.last_name,
        'token': auth.token,
        'last_login': new_user.last_login
    })

    return jsonify(response)

@app.route('/api/login', methods=['POST'])
def login():
    data = MultiDict(mapping=request.json)
    form = LoginForm(data, csrf_enabled=False)

    if not form.validate():
        return jsonify({'error': 'invalid email or password'})
    
    email = data.get('email')
    password = data.get('password')

    user = db.session.query(User).filter_by(email=data['email']).first()

    if not user:
        return jsonify({'error': 'email not found'})

    if not user.password == data.get('password'):
        return jsonify({'error': 'invalid credentials'})

    auth = user.tokens[0].__repr__()

    return jsonify({
        'id': user.id,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'token': auth.get('token'),
        'last_login': user.last_login
    })

@app.route('/api/logout')
def logout():
    return jsonify({
        'message': 'logout successful'
    })

@app.route('/api/users', methods=['GET'])
def user_details():
    data = MultiDict(mapping=request.json)
    return jsonify({})

###
# Routing for your application.
###
# @app.route('/login', methods=['GET', 'POST'])
# @oid.loginhandler
# def login():
#     if g.user is not None and g.user.is_authenticated():
#         return redirect(url_for('index'))
#     form = LoginForm()
#     print app.config['OPENID_PROVIDERS']
#     if form.validate_on_submit():
#         session['remember_me'] = form.remember_me.data
#         return oid.try_login(form.openid.data, ask_for=['nickname', 'email'])
#     return render_template('login.html', 
#                            title='Sign In',
#                            form=form,
#                            providers=app.config['OPENID_PROVIDERS'])

@app.route('/profile/', methods=['POST','GET'])
def profile_add():
    if request.method == 'POST':
        first_name = request.form['first_name']
        last_name = request.form['last_name']

        # write the information to the database
        newprofile = User(first_name=first_name,
                               last_name=last_name)
        db.session.add(newprofile)
        db.session.commit()

        return "{} {} was added to the database".format(request.form['first_name'],
                                             request.form['last_name'])

    form = ProfileForm()
    return render_template('profile_add.html',
                           form=form)

@app.route('/profiles/',methods=["POST","GET"])
def profile_list():
    profiles = User.query.all()
    if request.method == "POST":
        return jsonify({"age":4, "name":"John"})
    return render_template('profile_list.html',
                            profiles=profiles)

@app.route('/profile/<int:id>')
def profile_view(id):
    profile = User.query.get(id)
    return render_template('profile_view.html',profile=profile)


@app.route('/about/')
def about():
    """Render the website's about page."""
    return render_template('about.html')


###
# The functions below should be applicable to all Flask apps.
###

@app.route('/<file_name>.txt')
def send_text_file(file_name):
    """Send your static text file."""
    file_dot_text = file_name + '.txt'
    return app.send_static_file(file_dot_text)


@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=600'
    return response


@app.errorhandler(404)
def page_not_found(error):
    """Custom 404 page."""
    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0",port="8888")
